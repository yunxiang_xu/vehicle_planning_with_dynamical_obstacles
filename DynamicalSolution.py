from world import World
import numpy as np
from copy import deepcopy

MAX_LINEAR_V = 1.0
MIN_LINEAR_V = -0.2
MAX_ANGULAR_V = 1.57
MIN_ANGULAR_V = -1.57
SIM_STEP_HALF = 5;
class DynamicalSolution(object):
    def __init__(
        self,
        world,
        goal,
        dt,
        goal_tolerance
    ):
        self.world = world
        self.goal = goal
        self.dt = dt
        self.goal_tolerance = goal_tolerance
    
    # find next several commands using simulation
    def generateCommandsBuffer(self):
        vehicle_pos = self.world.get_vehicle().get_state()
        world_sim = self.world.get_snapshot()
        orders_buffer = []
        vehicle_sim = world_sim.get_vehicle();
        # simulation state:
        # - 0: moving forward
        # - 1: hit wall
        # - 2: hit ball
        # - 3: reach goal
        sim_state = 0
        for t in xrange(2 * SIM_STEP_HALF):
            vehicle_sim_pos = vehicle_sim.get_state()
            goal_sim_distance = np.linalg.norm(vehicle_sim_pos[:2] - self.goal)
            
            # check if collision in the simulation
            world_sim_size = world_sim.get_size()
            # check if hit the wall
            if vehicle_sim_pos[0] < 0 \
               or vehicle_sim_pos[1] < 0 \
               or vehicle_sim_pos[0] > world_sim_size[0] \
               or vehicle_sim_pos[1] > world_sim_size[1]:
                sim_state = 1
                break

            # check if hit some obstacle
            obstacles_sim = world_sim.get_obstacles()
            for i in xrange(len(obstacles_sim)):
                if obstacles_sim[i].in_collision(vehicle_sim_pos[:2]):
                    collision_pos = obstacles_sim[i].get_state()
                    sim_state = 2
                    ob_index = i
                    break
            # check if we have reached our goal
            if goal_sim_distance < self.goal_tolerance:
                sim_state = 3
                break
            
            (linear_velocity, angular_velocity) = \
               self.findCommandToGoal(vehicle_sim_pos, self.goal)
            vehicle_sim.set_commands(
                linear_velocity,
                angular_velocity
            )
            
            orders_buffer.append((linear_velocity, angular_velocity))
            
            world_sim.update(self.dt)
        
        if (sim_state == 0):
            orders_buffer = orders_buffer[:SIM_STEP_HALF]    
        elif (sim_state == 1):
            orders_buffer = []
            (linear_velocity, angular_velocity) = \
               self.findCommandToGoal(vehicle_pos, self.goal)
            linear_velocity = 0
            orders_buffer.append((linear_velocity, angular_velocity))
            
            
        # if hit ball, redo the simulation using other commands
        elif (sim_state == 2):
            
            orders_buffer = self.findCommandToAvoidObstacle(ob_index)
        
        # if reach goal, go!
        elif (sim_state == 3):
            pass

        return orders_buffer
    
    def findCommandToGoal(self, vehicle_pos, goal):
        [x, y, theta] = vehicle_pos
        gx, gy = goal
        distance = np.linalg.norm(vehicle_pos[:2]- goal)
        alpha = np.arctan2(gy-y,gx-x)
        angular_v = (alpha - theta)/2
        if abs(alpha - theta) > np.pi/2:
            linear_v = 0
        else:
            linear_v = MAX_LINEAR_V - 2 * abs(alpha-theta) / np.pi
        return (linear_v, angular_v)

    
    def findCommandToAvoidObstacle(self, ob_index):
        
        orders_buffer = []
  
        # see what will happen if we follow the avoid obstacle route
        world_sim = self.world.get_snapshot()
        vehicle_sim = deepcopy(self.world.get_vehicle())
        ob = deepcopy(self.world.get_obstacles()[ob_index])
        
        orders_buffer = self.avoidObstacle(vehicle_sim, ob)
        if orders_buffer:
            valid = True
            vehicle_sim = world_sim.get_vehicle()
            for i in xrange(len(orders_buffer)):
                if world_sim.in_collision():
                    valid = False;
                    break
                
                (linear_v, angular_v) = orders_buffer[i]
                vehicle_sim.set_commands(linear_v, angular_v)
                
                world_sim.update(self.dt)
            
            if valid: return orders_buffer
        
        # see what will happen if holds still
        world_sim = self.world.get_snapshot()
        vehicle_sim = world_sim.get_vehicle()
        valid = True
        for i in xrange(2 * SIM_STEP_HALF):
            vehicle_sim.set_commands(0.0, 0.0)
            
            if world_sim.in_collision():
                valid = False;
                break
            
            world_sim.update(self.dt)
        if valid:
            print 'stop to dodge'
            orders_buffer = [(0,0)]*SIM_STEP_HALF
        else:
            orders_buffer = []
        return orders_buffer
    

    # only care about the single vehicle and a single obstacle
    # probably not a good idea, need to be further discuessed
    def avoidObstacle(self, vehicle, ob):
        
        orders_buffer = []
        
        vehicle_pos = vehicle.get_state()
        world_size = self.world.get_size()
        [x, y, theta] = vehicle_pos
        ob_x, ob_y = ob.get_state()
        
        # see if the obstacles if one the other side of the world
        # if yes, move the obstacle to the equivalent position
        if abs(ob_x - x) > 4 * SIM_STEP_HALF * self.dt * MAX_LINEAR_V:
            ob_x -= np.sign(ob_x - x) * world_size[0]
        if abs(ob_y - y) > 4 * SIM_STEP_HALF * self.dt * MAX_LINEAR_V:
            ob_y -= np.sign(ob_y - y) * world_size[1]
        
        
        # calculate the obstacle's position wrt the vehicle
        distance = np.linalg.norm(np.array([x-ob_x,y-ob_y]))
        alpha = np.arctan2(ob_y - y, ob_x - x)
        ob_radius = ob.get_radius()
        linear_v = angular_v = 0
        ob_velocity = ob.get_velocity()
        
        
        # if it is a static obstacle: circle pass it
        # still has some bugs to fix
        if ob_velocity <= 0.1:
            # try two ways, left and right
            direction = [1.0, -1.0]
            for it in xrange(2):
                orders_buffer = []
                vehicle_sim = deepcopy(vehicle)
                valid = True
                for i in xrange(SIM_STEP_HALF):
                    vehicle_sim_pos = vehicle_sim.get_state()
                    
                    if vehicle_sim_pos[0] < 0 \
                       or vehicle_sim_pos[0]>world_size[0] \
                       or vehicle_sim_pos[1] < 0 \
                       or vehicle_sim_pos[1] > world_size[1]:
                        valid = False
                        break
                    # decide left or right
                    angular_v = direction[it] * MAX_ANGULAR_V
                    linear_v = 0.75 * MAX_LINEAR_V
                    vehicle_sim.set_commands(linear_v, angular_v)
                    vehicle_sim.update(self.dt)
                    orders_buffer.append((linear_v, angular_v))
                if valid:
                    print 'go left' if it == 0 else 'go right'
                    return orders_buffer
                else: 
                    orders_buffer = []
                    
        # if not a static obstacle,
        # simulate the vehicle and obstacle to find a path
        if not orders_buffer:
            direction = [1.0,0.0,-1.0,1.0,0.0,-1.0,0]
            velocity = [1.0,1.0,1.0,0.0,0.0,0.0,1.0]
            
            for it in xrange(7):
            # try careful turn to avoid
                vehicle_sim = deepcopy(vehicle)
                ob_sim = deepcopy(ob)
                valid = True
                orders_buffer = []
                for i in xrange(SIM_STEP_HALF):
                    ob_direction = ob_sim.get_direction()
                    vehicle_sim_pos = vehicle_sim.get_state()
                    if vehicle_sim_pos[0] < 0 \
                       or vehicle_sim_pos[0]>world_size[0] \
                       or vehicle_sim_pos[1] < 0 \
                       or vehicle_sim_pos[1] > world_size[1]:
                        valid = False
                        break
                    if ob_sim.in_collision(vehicle_pos[:2]):
                        valid = False
                        break
                    # slower and let obstacle pass
                    '''if np.cos(ob_direction) * np.cos(theta) + \
                       np.sin(ob_direction) * np.sin(theta) >= 0:
                        angular_v = np.sign(ob_direction - theta) * MIN_ANGULAR_V
                        linear_v = ob_sim.get_velocity() / 2.0
                    else:
                        angular_v = np.sign(alpha - theta) * MAX_ANGULAR_V
                        linear_v = MAX_LINEAR_V / 2.0'''
                    angular_v = MAX_ANGULAR_V*direction[it]
                    if velocity[it] > 0: linear_v = MAX_LINEAR_V * velocity[it]
                    else: linear_v = 0.5 * ob_sim.get_velocity()
                    vehicle_sim.set_commands(linear_v, angular_v)

                    orders_buffer.append((linear_v,angular_v))
                    vehicle_sim.update(self.dt)
                    ob_sim.update(self.dt)
                if valid: 
                    print it
                    return orders_buffer
                else: orders_buffer = []
        return orders_buffer
        
        

from world import World
import numpy as np
from rrt import ConnectRRT
from rrt import RRT
import pygame
from time import sleep
# init world
start_pose = np.array([0.5, 0.5, np.pi/2])
goal = np.array([9.5,9.5])
world_size = np.array([10,10])
px2m = 100
max_nodes = 4000

world = World(
    world_size = world_size,
    start_pose = start_pose,
    px2m = px2m
)
world.generate_obstacles(
    goal = goal,
    n_obstacles = 300,
    static = True
)
dt =0.1
world.update(dt)

screen = world.init_screen()

world.draw(screen)
explore = 1
searchPath = 0
if explore:
    random_tree = RRT(
        world,
        start_pose[:2],
        max_nodes = max_nodes,
        tolerance = 0.1
    ) 
        
    random_tree.build(screen = screen)
    pygame.image.save(screen, 'tree.png')
    
if searchPath:
    rrt = ConnectRRT(
        world,
        start_pose[:2],
        goal,
        tolerance = 0.1,
        max_nodes = max_nodes * 2
    )

    path = rrt.plan()

    l = len(path)
    for i in xrange(l-1):
        pygame.draw.line(
            screen,
            (255,225,255),
            path[i]*px2m, 
            path[i+1]* px2m
        )

    pygame.image.save(screen, 'map.png')


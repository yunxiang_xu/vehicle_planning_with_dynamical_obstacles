'''
fully-observable world with dynamic obstacles
'''

import numpy as np
import pygame
from copy import copy, deepcopy


class Obstacle(object):

    def __init__(
        self,
        world_size,
        radius=0.2,
        color=(255.0, 0.0, 0.0),
        max_velocity=1.0,
        static=False
    ):
        self._radius = radius
        self._color = color
        self._world_size = world_size
        self._static = static

        # select random direction and velocity
        self._position = np.random.random(2) * list(world_size)
        self._direction = np.random.random() * 2.0*np.pi

        # initialize obstacle velocity
        self._velocity = 0.0
        if not self._static:
            self._velocity = np.random.random() * max_velocity

    def get_velocity(self):
        return self._velocity

    def get_direction(self):
        return self._direction

    def get_radius(self):
        return self._radius

    def set_state(
        self,
        position,
        direction,
        velocity
    ):
        '''
        manual state override
        '''
        self._position = position
        self._direction = direction
        self._velocity = velocity

    def get_state(self):
        return self._position

    def in_collision(self, point, radius=None):
        if radius is None:
            radius = self._radius

        distance = np.linalg.norm(point - self._position)
        if distance < radius:
            return True
        return False

    def update(self, dt):
        '''
        updates state of obstacle
        '''
        self._position[0] += np.cos(self._direction) * self._velocity * dt
        self._position[1] += np.sin(self._direction) * self._velocity * dt

        # wrap
        self._position = np.mod(self._position, self._world_size)

    def draw(self, screen, px2m):
        pygame.draw.circle(
            screen,
            self._color,
            [int(self._position[0] * px2m), int(self._position[1] * px2m)],
            int(self._radius * px2m)
        )


class Vehicle(object):
    '''
    Differential Drive Vehicle Model

    _pose: [x, y, theta] in [meters, meters, radians]
    _linear_velocity: m/s
    _angular_velocity: rad/s
    '''
    def __init__(
        self,
        initial_pose,
        initial_linear_velocity=0.0,
        initial_angular_velocity=0.0,
        color=(0, 255, 0)
    ):
        self.color = color
        self._pose = np.array(initial_pose)
        self._linear_velocity = initial_linear_velocity
        self._angular_velocity = initial_angular_velocity
        self._trace = [[self._pose[0], self._pose[1]]]

    def set_commands(
        self,
        linear_velocity,
        angular_velocity
    ):
        '''
        Set input commands
            linear_velocity: m/s
            angular_velocity: rad/s
        '''
        self._linear_velocity = linear_velocity
        self._angular_velocity = angular_velocity

    def get_state(self):
        '''
        Return pose where pose is [x, y, theta]
        '''
        return copy(self._pose)

    def update(
        self,
        dt,
        eps=1e-12
    ):
        x, y, theta = self._pose

        self._trace.append([x, y])

        # update model
        if np.abs(self._angular_velocity) < eps:
            direction = theta + self._angular_velocity * 0.5 * dt
            x += self._linear_velocity * np.cos(direction) * dt
            y += self._linear_velocity * np.sin(direction) * dt
        else:
            old_theta = theta
            radius = self._linear_velocity/self._angular_velocity
            theta = theta + self._angular_velocity * dt
            x += radius * (np.sin(theta) - np.sin(old_theta))
            y -= radius * (np.cos(theta) - np.cos(old_theta))

        self._pose = np.array([x, y, theta])

    def draw(self, screen, px2m):
        # draw a triangle representing vehicle
        base_coords = np.array([
            [0.0, -0.05],
            [0.0, 0.05],
            [0.15, 0.0],
            [0.0, -0.05]
        ])

        theta = self._pose[2]
        rot = np.array([[np.cos(theta), -np.sin(theta)],
                        [np.sin(theta), np.cos(theta)]])
        coords = np.dot(base_coords, rot.T)
        pygame.draw.lines(
            screen,
            self.color,
            True,
            np.int32((coords + self._pose[:2]) * px2m)
        )

        # draw trace
        pygame.draw.lines(
            screen,
            [255, 255, 255],
            False,
            np.int32(np.array(self._trace) * px2m)
        )


class World(object):

    def __init__(
        self,
        world_size=np.array([5.0, 5.0]),
        start_pose=np.array([1.0, 1.0, 0.0]),
        px2m=100
    ):
        self.size = world_size
        self.px2m = px2m

        self._start_pose = start_pose
        self.vehicle = Vehicle(start_pose)
        self._obstacles = []

    def generate_obstacles(
        self,
        goal=None,
        n_obstacles=10,
        obstacle_radius=0.2,
        static=False,
        max_object_velocity=1.0,
    ):
        # can be used to ensure that no obstacles exist near goal
        if goal is None:
            goal = self._start_pose[:2]
        # randomly generate obstacle field
        self._obstacles = []
        for _ in xrange(0, n_obstacles):
            # resample obstacle until it isn't in collision
            while True:
                obstacle = Obstacle(
                    self.size,
                    radius=obstacle_radius,
                    static=static,
                    max_velocity=max_object_velocity
                )
                if not obstacle.in_collision(
                    self._start_pose[:2],
                    radius=5.0 * obstacle_radius
                ) and not obstacle.in_collision(
                    goal,
                    radius=2.5 * obstacle_radius
                ):
                    break

            self._obstacles.append(obstacle)

    def set_obstacles(self, obstacles):
        # manually set obstacle field
        self._obstacles = obstacles

    def init_screen(self):
        pygame.init()
        screen = pygame.display.set_mode(
            np.int32(self.size * self.px2m)
        )
        pygame.display.set_caption('obstacle field')

        screen.fill((0, 0, 30))
        pygame.display.flip()
        return screen

    def get_obstacles(self):
        return self._obstacles

    def get_vehicle(self):
        return self.vehicle

    def get_size(self):
        return self.size

    def in_collision(self):
        vehicle_pos = self.vehicle.get_state()[:2]

        # check world bounds
        if vehicle_pos[0] < 0 \
           or vehicle_pos[1] < 0 \
           or vehicle_pos[0] > self.size[0] \
           or vehicle_pos[1] > self.size[1]:
            return True

        # check obstacle collisions
        for obstacle in self._obstacles:
            if obstacle.in_collision(vehicle_pos):
                return True

        return False

    def update(self, dt):
        self.vehicle.update(dt)
        for obstacle in self._obstacles:
            obstacle.update(dt)

    def draw(self, screen):
        screen.fill((0, 0, 30))
        self.vehicle.draw(screen, self.px2m)
        for obstacle in self._obstacles:
            obstacle.draw(screen, self.px2m)
        pygame.display.flip()

    def get_snapshot(self):
        return deepcopy(self)


if __name__ == "__main__":
    world = World()
    screen = world.init_screen()

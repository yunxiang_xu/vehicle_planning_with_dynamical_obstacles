from world import World
import numpy as np
from copy import deepcopy
from rrt import ConnectRRT

MAX_LINEAR_V = 1.0
MIN_LINEAR_V = -0.2
MAX_ANGULAR_V = 1.57
MIN_ANGULAR_V = -1.57
SIM_STEP_HALF = 5;

class StaticSolution(object):
    def __init__(
        self,
        world,
        goal,
        dt,
        goal_tolerance
    ):
        self.world = world
        self.goal = goal
        self.dt = dt
        self.goal_tolerance = goal_tolerance
    
    # Based on a static map, find a path from vehicle position to goal
    def generatePath(self, world_sim, goal):
        start = world_sim.get_vehicle().get_state()[:2]
        planner = ConnectRRT(
            world_sim,
            start,
            goal,
            self.goal_tolerance
        )
        path = planner.plan()
        return path
        
    def generateCommands(self, max_trial):
        world_sim = self.world.get_snapshot()
        commands = []
        finalPath = [self.world.get_vehicle().get_state()[:2]]
        for i in xrange(max_trial):
            path = self.generatePath(world_sim, self.goal)
            #print path
            if not path: continue
            num_wpts = len(path)
            it = 1
            while it < num_wpts:
                world_sim_sim = world_sim.get_snapshot()
                vehicle_sim_sim = world_sim_sim.get_vehicle()
                valid = True
                tempCommands = []
                while True:
                    vehicle_pos = vehicle_sim_sim.get_state()
                    wpt_distance = np.linalg.norm(vehicle_pos[:2] - path[it])
                    if world_sim_sim.in_collision():
                        valid = False
                        print 'collision'
                        break
                    if wpt_distance < self.goal_tolerance:
                        break
                    (linear_v, angular_v) = self.findCommandToGoal(
                        vehicle_pos,
                        path[it]
                    )
                    if np.abs(angular_v)>np.pi/2: linear_v = 0
                    vehicle_sim_sim.set_commands(linear_v, angular_v)
                    tempCommands.append((linear_v, angular_v))
                    world_sim_sim.update(self.dt)
                
                if valid:
                    commands += tempCommands
                    finalPath.append(path[it])
                    it += 1;
                    world_sim = world_sim_sim
                else: break
            if it == num_wpts: return commands, finalPath
        return [],[]
    
        
    # find Commands towards the goal without considering the obstacles
    def findCommandToGoal(self, vehicle_pos, goal):
        [x, y, theta] = vehicle_pos
        gx, gy = goal
        distance = np.linalg.norm(vehicle_pos[:2]- goal)
        alpha = np.arctan2(gy-y,gx-x)
        
        if abs(alpha - theta) > np.pi/90.0:
            angular_v = np.sign(alpha - theta) * \
                        min(MAX_ANGULAR_V, abs(alpha - theta)*2)
            if abs(alpha - theta) > np.pi/2:
                linear_v = 0
            else:
                linear_v = 0.1 - abs(alpha-theta) / np.pi / 5
        else:
            angular_v = (alpha - theta)
            linear_v = min(MAX_LINEAR_V,distance )
        
        '''
        angular_v = (alpha - theta)/2
        if abs(alpha - theta) > np.pi/2:
            linear_v = 0
        else:
            linear_v = MAX_LINEAR_V - 2 * abs(alpha-theta) / np.pi
        '''
        return (linear_v, angular_v)

                
                
                
                
                
                
                
                
                
        
        
        
        

# RRT for world object
# or image?

from world import World
import numpy as np
import sys
import math
import time
import pygame

SIG_TRAPPED = 0
SIG_ADVANCED = 1
SIG_REACHED = 2


# For tuples
class RRT():
    def __init__(self,
                 world,
                 start_pos,
                 tolerance = 0.25,
                 max_nodes=500,
                 px2m = 100
                 ):
        self.world = world
        self.obstacles = world.get_obstacles()
        self.tolerance = tolerance
        self.start_pos = start_pos
        self.px2m = px2m
        self.max_nodes = max_nodes
        self.tree_nodes = [self.start_pos]
        self.tree_parents = [0]
        self.tree_numnodes = 1

        


    # TODO:Change to a KDTree with a buffer list once list crosses 500 nodes
    def findNearestNode(self,q):
        closest = float("inf")
        nearest_node = None
        closest_idx = -1
        for idx in xrange(self.tree_numnodes):
            node = self.tree_nodes[idx]
            dist = np.linalg.norm(q - node)
            if dist < closest:
                closest = dist
                nearest_node = node
                closest_idx = idx
        return closest_idx,nearest_node
   
    def extend(self, end, screen = None):
        flag = SIG_REACHED
        start_idx, start = self.findNearestNode(end)
        # have direction vector and normal vector
        direct_v = end - start
        direct_v = direct_v / np.linalg.norm(direct_v)
        normal_v = np.array([start[1] - end[1], end[0] - start[0]])
        normal_v = normal_v / np.linalg.norm(normal_v)
        # compute the length from start to end
        length = np.linalg.norm(end-start)
        for ob in self.obstacles:
            # get the position and radius of the obstacle
            ob_pos = ob.get_state()
            ob_r = ob.get_radius() + self.tolerance/2.0
            
            # if the obstacle is on the other side, do not worry about
            if np.dot(ob_pos - start, end - start) < 0: continue
            dist = np.abs(np.dot(normal_v, end - ob_pos))
            
            # if the distance from obstacle to the line is bigger than r,
            # do not worry
            if dist > ob_r : continue
            
            #compute distance from start node to the obstacle
            distToOb = np.dot(ob_pos - start, direct_v) - \
                       np.sqrt(ob_r * ob_r - dist * dist)
            # if the obstacle is farther than end, do not worry about
            if distToOb > length: continue
            # change the flag because will not reach
            flag = SIG_ADVANCED
            # set new length and end
            length = distToOb - self.tolerance
            end = start + direct_v * length
            
        if length < self.tolerance: return SIG_TRAPPED
        else:
            self.tree_nodes.append(end)
            self.tree_parents.append(start_idx)
            self.tree_numnodes += 1
            if screen:
                pygame.draw.line(
                    screen, 
                    (255,255,255),
                    start * self.px2m, 
                    end * self.px2m
                )
            #print end
            return flag
            

    def build(self, screen = None):
        world_size = self.world.get_size()
        time_begin = time.time()
        for i in xrange(self.max_nodes):
            samp_node = np.random.rand(2) * world_size
            #print samp_node,
            self.extend(samp_node, screen = screen)
        time_end = time.time()
        print 'Total time = ',time_end-time_begin
        
    # Get path from some certain node
    def getPath(self):
        leaf_idx = self.tree_numnodes - 1
        node = self.tree_nodes[leaf_idx]
        path = [node]
        node_idx = leaf_idx
        parent_node_idx = self.tree_parents[node_idx]
        while node_idx!=parent_node_idx:
            node = self.tree_nodes[parent_node_idx]
            node_idx = self.tree_parents[node_idx]
            parent_node_idx = self.tree_parents[node_idx]
            path.append(node)
        return path

    # Get sparsified path based on line of sight
    # A node will try and connect to the last node in the RRT path that
    # it can see.



class ConnectRRT():
    def __init__(
        self,
        world,
        start,
        goal,
        tolerance = 0.25,
        max_nodes=1000,
        px2m = 100
    ):
        self.start_rrt = RRT(
            world,
            start,
            tolerance,
            max_nodes,
            px2m
        )
        self.goal_rrt = RRT(
            world,
            goal,
            tolerance,
            max_nodes,
            px2m
        )
        self.world_size = world.get_size()
        self.obstacles = world.get_obstacles()
        self.max_nodes = max_nodes
        self.tolerance = tolerance
    def smooth(self, path):
        if not path: return []
        newpath = [path[0]]
        last = 0
        cur = 2
        while cur <= len(path):
            if cur < len(path) and self.isValid(newpath[-1],path[cur]):
                cur += 1
            elif cur == len(path):
                newpath.append(path[cur-1])
                cur += 1
            else:
                direct_v = path[cur] - path[cur-1]
                for i in xrange(5):
                    newnode = path[cur-1] + direct_v * (4.0-i) / 5.0
                    if self.isValid(newnode, newpath[-1]):
                        newpath.append(newnode)
                        cur += 1
                        break
        return newpath
        
    def isValid(self, node1, node2):
        normal_v = np.array([node1[1] - node2[1], node2[0] - node1[0]])
        normal_v = normal_v / np.linalg.norm(normal_v)
        for ob in self.obstacles:
            ob_pos = ob.get_state()
            ob_r = ob.get_radius() + self.tolerance / 2.0
            if np.dot(ob_pos - node1, node2 - node1) < 0 or \
               np.dot(ob_pos - node2, node1 - node2) < 0:
                continue
            dist = np.abs(np.dot(normal_v, node2 - ob_pos))
            if dist < ob_r: return False
        return True

    def plan(self):
        start = True
        tree_a = self.start_rrt
        tree_b = self.goal_rrt
        time_begin = time.time()
        path = []
        for i in xrange(self.max_nodes):
            tree_a,tree_b = tree_b,tree_a
            start = not start
            samp_node = np.random.rand(2) * self.world_size
            if tree_a.extend(samp_node):
                new_node = tree_a.tree_nodes[-1]
                if (tree_b.extend(new_node) == SIG_REACHED):
                    # Generate path from the trees
                    #print 'We are done!!'
                    tmp  = tree_b.getPath()
                    tmp2 = tree_a.getPath()
                    if start:
                        tmp2.reverse()
                        #tmp2.extend(tmp[1:])
                        path += tmp2
                        path += tmp[1:]
                    else:
                        tmp.reverse()
                        #tmp.extend(tmp2[1:])
                        path += tmp
                        path += tmp2[1:]
                    break
                    
        path = self.smooth(path)
        path.reverse()
        path = self.smooth(path)
        path.reverse()
        if not path:
            print 'can not find a path'
        time_end = time.time()
        print 'Total time =', time_end - time_begin
        return path

